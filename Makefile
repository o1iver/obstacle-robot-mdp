SHELL := /bin/bash

BASEDIR=$(shell pwd)
PROJECT=$(shell basename $(BASEDIR))
VERSION=$(shell cat VERSION)
# Feel free to rewrite this (way too ugly)
AUTHORS=$(shell cat AUTHORS | grep - | cut -d ' ' -f 2- | tr '\n' ',' |\
	sed 's/,/, /g' | sed 's/, \?$$//')

DISTPKG="$(PROJECT)-$(VERSION).tar.gz"

# Documentation:
DOCSPROJDIR=./docs
DOCSTRGTDIR=./docs/_build


info:
	@echo "Project: $(PROJECT)"
	@echo "Version: $(VERSION)"
	@echo "Authors: $(AUTHORS)"
	@echo "Basedir: $(BASEDIR)"
	@echo "Distpkg: $(DISTPKG)"
	@echo "Documentation project: $(DOCSPROJDIR)"
	@echo "Documentation: $(DOCSTRGTDIR)"
project:
	@echo "$(PROJECT)"
version:
	@echo "$(VERSION)"
authors:
	@echo "$(AUTHORS)"
dist:
	@echo "Creating distribution package..."
	tar -czf $(DISTPKG) *
test:
	@echo "Running tests..."
	python2 -m unittest discover
docs:
	@echo "Creating documentation..."
	test -d $(DOCSPROJDIR) || make docs-proj
	sphinx-build2 -b html $(DOCSPROJDIR) $(DOCSTRGTDIR)
docs-proj:
	@echo "Creating API documentation project..."
	sphinx-apidoc2 --full -A "$(AUTHORS)" -V "$(VERSION)" -H "$(PROJECT)" -o $(DOCSPROJDIR) $(PROJECT)
clean:
	@echo "Removing *.pyc files..."
	find . -name "*.pyc" -exec rm -rf {} \;
	@echo "Removing dist package..."
	rm -f $(DISTPKG)
	@echo "Removing built documentation..."
	rm -rf $(DOCSTRGTDIR)
clean-docs:
	@echo "Removing documentation at: $(DOCSPROJDIR)"
	rm -rf $(DOCSPROJDIR)
	

.PHONY: docs
