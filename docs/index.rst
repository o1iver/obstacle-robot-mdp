.. orm documentation master file, created by
   sphinx-quickstart2 on Fri May 18 12:46:16 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to orm's documentation!
===============================

Contents:

.. toctree::
   :maxdepth: 4

   orm


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

