import argparse

import orm.grid
import orm.model
import orm.simulation
import orm.mdp



def create_problem(save_folder,size_x,size_y,num_obstacles,dest_x,dest_y,discount_factor):

    fp_grid_image = save_folder+'/grid.jpeg'
    fp_mdp_file   = save_folder+'/prob.MDP'

    print('Problem folder: %s' % save_folder)
    print('Grid image:     %s' % fp_grid_image)
    print('MDP file:       %s' % fp_mdp_file)

    destination = orm.model.Coordinate(dest_x,dest_y)


    print('Creating simulation...')
    sim = orm.simulation.Simulation.random_simulation(size_x,size_y,
            num_obstacles,orm.model.Coordinate(0,0),destination)

    grid = sim.grid
    grid.draw(save_folder+'/grid.jpeg')

    print('Creating mdp...')
    mdp = orm.mdp.create_mdp(grid,destination,discount_factor)
    with open(fp_mdp_file,'w') as f:
        mdp.toCassandraFile(f)
    return mdp



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create ORM problem')
    parser.add_argument('save_folder', type=str, help='name of folder to save problem files to')
    parser.add_argument('size_x', type=int, help='size of grid in x-dimension')
    parser.add_argument('size_y', type=int, help='size of grid in y-dimension')
    parser.add_argument('num_obstacles', type=int, help='number of obstacles')
    parser.add_argument('dest_x', type=int, help='x-coordinate of desination')
    parser.add_argument('dest_y', type=int, help='y-coordinate of desination')
    parser.add_argument('-d','--discount-factor', type=float, default=0.98, help='discount factor for infinite horizon value iteration')

    args = parser.parse_args()
    create_problem(**vars(args))

