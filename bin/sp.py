import os

import pierre.io.signaling

DEFAULT_CONFIG = {
        'PIERRE_SERVICES_ADDR': None,
        'PIERRE_TIMEMOUT': None }

config = dict(map(lambda x: (x[0],os.getenv(x[0],x[1])),DEFAULT_CONFIG.items()))

print("Configuration:")
print(config)


# Signal processor:

PLOT_FNAMES = {'orm.sim.pos_x': '/home/ost/dev/tmp/sigplots/orm_sim_pos_x.png',
               'orm.sim.pos_y': '/home/ost/dev/tmp/sigplots/orm_sim_pos_y.png',
               'orm.sim.last_reward':'/home/ost/dev/tmp/sigplots/orm.sim.last_reward.png',
               'orm.sim.total_reward': '/home/ost/dev/tmp/sigplots/orm.sim.total_reward.png'}

plotting_sig_proc = pierre.io.signaling.PlottingSignalProcessor(plot_fnames=PLOT_FNAMES)

SIG_PROC_IDENT = 'orm.sig_processor'

sig_proc_service = pierre.io.signaling.SignalProcessorService(SIG_PROC_IDENT,plotting_sig_proc)
sig_proc_service.serve()





