import re
import sys

fname  = sys.argv[1]
size_x = int(sys.argv[2])
size_y = int(sys.argv[3])

name="""
name: ORMIOInterface
"""

sigproc="""
signal_processor:
    type: SignalProcessorClient
    parameters:
        identity: orm.sig_processor
"""

signals="""
signals:
    -   name: pos_x
        type: UnsignedIntegerSignal
        parameters:
            alias: orm.sim.pos_x
            max: %i
            read_only: True
            start_value: 0
    -   name: pos_y
        type: UnsignedIntegerSignal
        parameters:
            alias: orm.sim.pos_y
            max: %i
            read_only: True
            start_value: 0
    -   name: move
        type: StringSignal
        parameters:
            alias: orm.sim.move
            start_value: 'Dont'
            choices: ['Dont','North','NorthEast','East','SouthEast','South','SouthWest','West','NorthWest']
""" % (size_x-1,size_y-1)

actions="""
actions:
    0:
        name: Dont
        values:
            move: Dont
    1:
        name: North
        values:
            move: North
    2:
        name: NorthEast
        values:
            move: NorthEast
    3:
        name: East
        values:
            move: East
    4:
        name: SouthEast
        values:
            move: SouthEast
    5:
        name: South
        values:
            move: South
    6:
        name: SouthWest
        values:
            move: SouthWest
    7:
        name: West
        values:
            move: West
    8:
        name: NorthWest
        values:
            move: NorthWest
"""


it_ = ((y,x) for y in xrange(size_y) for x in xrange(size_x))

states = """
states:
"""

n = -1
for x,y in it_:
    n += 1
    states += ("\t%i:\n\t\tname: %s\n\t\tpredicates:\n\t\t\t- \"pos_x == %i\"\n\t\t\t- \"pos_y == %i\"\n" %
            (n,'state_'+str(x)+'_'+str(y),x,y))

states = states.replace('\t','    ')
        


with open(fname,'w') as f:
    writeline = lambda x: f.write(x+'\n')
    writeline(name)
    writeline(sigproc)
    writeline(signals)
    writeline(actions)
    writeline(states)


