# -*- coding: utf-8 -*-
"""
    orm.grid
    ~~~~~~~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import orm.util as util

import re
import collections
import itertools

from pprint import saferepr

class Grid(object):
    """
    N-dimensional grid.
    """
    @staticmethod
    def null_grid(null_value,*sizes):
        if len(sizes) in (0,1):
            raise ValueError("cannot create %id grid" % len(sizes))
        return Grid(util.create_null_array(null_value,*sizes))

    def __init__(self,grid):
        """
        Create a grid given the n-d array grid.
        """
        try:
            dims = util.array_dimensions(grid)
        except ValueError:
            raise ValueError("grid array should be nice :)")

        self.grid = grid

    def __len__(self):
        """
        Returns the number of elements in the grid.
        """
        return util.mult(self.sizes)

    def __iter__(self):
        """
        Returns an iterator of tuples (x0,x1,...,xN) where x0,x1,...,xN are
        the coordinates of the elements.
        """
        gens = map(range,self.sizes)
        return itertools.product(*gens)

    def __repr__(self):
        return saferepr(self.grid)
    
    def __getitem__(self,x):
        """
        Get the first dimension sub-array (or element).
        """
        return self.grid[x]
    
    def __setitem__(self,x,ys):
        """
        Set the first dimension sub-array (or element).
        """
        self.grid[x] = ys

    def __getattr__(self,name):
        """
        A little bit of magic for easier use: the size of the grid in a certain
        dimension can be accessed using 'size_d' attributes where d is a natural
        number (incl. zero) specifying the dimension. Additionally the first
        three dimensions 0,1 and 2 can be accessed using 'size_x', 'size_y' and
        'size_z'. If the requested dimension does not exist a traditional 
        AttributeError is raised.
        """
        # Enable default cartesian names
        if name == "size_x": name = "size_0"
        if name == "size_y": name = "size_1"
        if name == "size_z": name = "size_2"
        if name.startswith('size_'):
            pattern = re.compile("size_(?P<dim>\d+)")
            match = pattern.match(name)
            if match:
                dim = int(match.groupdict()["dim"])
                if dim < self.dimensions: 
                    return self.sizes[dim]
        raise AttributeError("'%s' object has no attribute '%s'"
                % (self.__class__.__name__,name))
    
    @property
    def sizes(self):
        """
        Returns a tuple of each dimensions size.
        """
        return util.array_dimensions(self.grid)

    @property
    def dimensions(self):
        """
        Returns the number of dimensions of this grid.
        """
        return len(self.sizes)

    def outside(self,*coords):
        """
        Function that returns True if any of the coordinates are outside the
        grid limits (either negative or greater than limit).
        """
        if not hasattr(self,"_out_funcs"):
            self._out_funcs = map(lambda m: lambda x: x >= m or x < 0,
                    self.sizes)
        return any(map(lambda x:x[0](x[1]),zip(self._out_funcs,coords)))

    def neighbours(self,*coords):
        """
        Returns a list of tuples with the coordinates of surrounding elements.
        """
        if len(coords) != self.dimensions:
            raise ValueError("coordinate dimension (%i) does not match grid"\
                    "dimension (%i)" % (len(coords),self.dimensions))

        delta_cs = (-1,0,1)
        deltas = itertools.product(*([delta_cs]*self.dimensions))
        neighbours = []
        for delta in deltas: neighbours.append(map(sum,zip(coords,delta)))

        neighbours = filter(lambda x: util.not_func(self.outside)(*x),
                neighbours)
        return map(tuple,neighbours)

    def draw(self,fname,grid_dim=None,tt_font=None,font_size=None):
        """
        Draw a grid image and save at fname. The grid_dim optional argument can
        be used to specify the length of the side of each grid element's square.
        The string value of the grid element is drawn inside each square.
        """
        import Image
        import ImageDraw
        import ImageFont

        tt_font = tt_font or "/usr/share/fonts/TTF/FreeMono.ttf"
        font_size = font_size or 20

        grid_dim = grid_dim or 25

        if self.dimensions != 2:
            raise RuntimeError("can only draw 2d grids")

        width  = grid_dim*self.size_x
        height = grid_dim*self.size_y

        image = Image.new("RGBA",(width,height),(255,255,255,255))
        drawi = ImageDraw.Draw(image)
        try:
            font  = ImageFont.truetype(tt_font,font_size)
        except IOError:
            font = None

        for x,y in self:
            v = self[x][y]
            square_coords_ul = (grid_dim*x,    grid_dim*y)
            square_coords_lr = (grid_dim*(x+1),grid_dim*(y+1))
            drawi.rectangle((square_coords_ul,square_coords_lr),
                    outline=(0,0,0,0))
            ul_x,ul_y = square_coords_ul[0],square_coords_ul[1]
            lr_x,lr_y = square_coords_lr[0],square_coords_lr[1]

            txt_width,txt_height = drawi.textsize(unicode(v),font=font)
            txt_pos_x = (ul_x + (lr_x-ul_x)/2.0) - txt_width/2.0
            txt_pos_y = (ul_y + (lr_y-ul_y)/2.0) - txt_height/2.0

            drawi.text((txt_pos_x,txt_pos_y),unicode(v),font=font,fill=(0,0,0,0))

        image.save(fname)












