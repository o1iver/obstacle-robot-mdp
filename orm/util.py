# -*- coding: utf-8 -*-
"""
    orm.util
    ~~~~~~~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import operator

utf_arrows = [u'↺',u'↑',u'↗',u'→',u'↘',u'↓',u'↙',u'←',u'↖']

def mult(iter):
    return reduce(operator.mul,iter)


def not_func(func):
    def inner(*args,**kwargs):
        return not func(*args,**kwargs)
    return inner

notFunc = not_func

def initialize(obj,**kwargs):
    """
    Initializes an object with the keyword arguments provided in the call.
    """
    for k,v in kwargs.items():
        obj.__dict__[k] = v

def numInIter(iter_,obj):
    """
    Returns the number of times an object is in an iterator object by comparing
    the items' IDs with the object's ID.
    """
    fpred = lambda x: id(x) == id(obj)
    return len(filter(fpred,iter_))

num_in_iter = numInIter

def inIter(iter_,obj):
    """
    Checks whether or not is returned by an iterator by comparing the IDs of
    the returned objects with the ID of the object.
    """
    return not (numInIter(iter_,obj) == 0)

in_iter = inIter
    
def createNullArray(null,*args):
    """
    Recursively creates a N-dimensional list with n0,n1,...nN null elements.
    """
    if not all(map(lambda x: type(x) == int,args)):
        raise ValueError("Dimensions must be integers!")
    if filter(lambda x: x < 1,args):
        raise ValueError("Dimensions must be greater or equal to 1!")
    l = []
    if len(args) == 1:
        l = [null]*int(args[0])
    else:
        for n in range(args[0]): l.append(createNullArray(null,*args[1:]))
    return l

create_null_array = createNullArray

def createNullArrayFunc(null):
    """
    Creates a create_null_array function with a pre-set null value.
    """
    return lambda *args: createNullArray(null,*args)

create_null_array_func = createNullArrayFunc
    
def array_dimensions(array,dims=None):
    """
    Given a n-dimensional array, where all arrays of each dimensions are of the
    same length, this function will return a tuple of the array sizes of all
    dimensions.

    Example:
        >> get_dimensions([[[1,2],[3,4],[5,6]],[[7,8],[9,10],[11,12]]])
        (2, 3, 2)
    """
    if all(map(lambda a: type(a) not in (list,set,tuple),array)):
        dims.append(len(array))
        return tuple(dims)

    if not all(map(lambda a: type(a) in (list,set,tuple),array)):
        raise ValueError("Sub-arrays not all of type list, set or tuple!")

    lens = map(len,array)
    if len(set(lens)) != 1:
        raise ValueError("Sub-arrays not of same length!")

    if not dims:
        dims = []

    dims.append(len(array))
    return array_dimensions(array[0],dims)

def cycle(l,n=1,right=True):
    """
    Returns a list cycled to either the right (default) or to the left by n
    steps (default n=1).
    """
    if right: return l[-n:] + l[:-n]
    else:     return l[ n:] + l[: n]


