# -*- coding: utf-8 -*-
"""
    orm.model
    ~~~~~~~~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
    
    This module provides the obstacle robot problem's model, meaning the
    underlying dynamics that define the way the robot can move on the grid
    and the rewards it may receive for such movements.
"""
import orm.grid as _grid
import orm.util as util

import copy
import codecs
import collections


_PROBABILISTIC_ = True

Coordinate = collections.namedtuple("Coordinate",('x','y'))

# ------------------------------------------------------------------------------
# Probabilistic movement

class Moves:
    """
    Enum type class with the robot's possible movements. Values are
    (delta-x,delta-y) tuples where delta-x and delta-y are the coordinate
    changes in the x and y direction respectively.
    """
    Dont      = ( 0, 0)
    North     = ( 0,-1)
    NorthEast = ( 1,-1)
    East      = ( 1, 0)
    SouthEast = ( 1, 1)
    South     = ( 0, 1)
    SouthWest = (-1, 1)
    West      = (-1, 0)
    NorthWest = (-1,-1)

#: List of values of enum type Moves (used to find indexes for the cyclic
#: rotation required to compute the movement probabilities).
moves        = [Moves.Dont,Moves.North,Moves.NorthEast,Moves.East,
                Moves.SouthEast,Moves.South,Moves.SouthWest,Moves.West,
                Moves.NorthWest]

#: String representation of moves (see move_from_string)
move_strings = ["Dont","North","NorthEast","East",
                "SouthEast","South","SouthWest","West",
                "NorthWest"]

def move_from_string(str_):
    """
    Given a string representation of string (as defined in move_strings), this
    function will return the actual value of a move (see enum-type class Moves).
    """
    try:
        return moves[move_strings.index(str_)]
    except ValueError:
        raise ValueError("no move named `%s'" % str_)

def move_to_string(move):
    """
    Given a move of type Moves, this function will return its string
    representation, as defined in move_strings.
    """
    return move_strings[moves.index(move)]

#: The probability of not moving if a move is defined.
random_num_no_move_probability        =  0.0200

#: The probability of moving in first the chosen direction and then the other
#: possible directions, clockwise from the chosen direction.
random_num_move_probabilities         = [0.7000,0.0900,0.0375,0.0100
                                        ,0.0050,0.0100,0.0375,0.0900]

#: The probability of the Moves.Dont move succeeding, currently it is equal to
#: one, meaning that the robot will always successfully not move.
num_stay_move_probability               =  1.0000

#: The deterministic probability of not moving if a move was chosen (obviously
#: it is equal to 0.0).
deterministic_num_no_move_probability =  0.0000

#: The deterministic probabilities of moving in a direction other than the one
#: chosen (obviously all 0.0, except for the chosen move).
deterministic_num_move_probabilities  = [1.0000,0.0000,0.0000,0.0000
                                        ,0.0000,0.0000,0.0000,0.0000]

if _PROBABILISTIC_:
    num_no_move_probability = random_num_no_move_probability
    num_move_probabilities  = random_num_move_probabilities
else:
    num_no_move_probability = deterministic_num_no_move_probability
    num_move_probabilities  = deterministic_num_move_probabilities


def move_destination(coord,move):
    """
    Given a coordinate and a move (as defined above), a new updated coordinate
    is returned.
    """
    return Coordinate(coord.x+move[0],coord.y+move[1])

def move_probabilities(move):
    """
    Given a move this function returns a dictionary {Move: probability} with the
    probabilities of the robot moving in the given directions.
    """
    if move is Moves.Dont:
        return dict([(Moves.Dont,num_stay_move_probability)] + 
                map(lambda x: (x,0.0),moves[1:]))
    else:
        probs = util.cycle(num_move_probabilities,moves[1:].index(move))
        probs = zip(moves[1:],probs)
        probs = [(Moves.Dont,num_no_move_probability)] + probs
        return dict(probs)

def theoretical_destination_probabilities(pos,move):
    """
    Given a position and a move, this function returns a dictionary
    {Coordinate: probability} of possible destinations and the probabilities
    of reaching them.
    """
    probs = move_probabilities(move)
    dests = map(lambda x: move_destination(pos,x),probs)
    return dict(zip(dests,probs.values()))

# ------------------------------------------------------------------------------
# Grid movement

class Squares:
    """
    Enum type class for grid square types. It is used to decide if the robot
    can move to a certain square or not and what rewards it receives for moving
    to the given square (see grid_rewards). Note that the Squares.Outside square
    type is only theoretical as a square outside of the grid is not possible; it
    is here to facilitate the reward calculation (and so that the reward for
    leaving the grid, ie. hitting a wall or falling of a cliff, can easily be
    defined in the same dictionary as all the other grid rewards.
    """
    Empty       = " "
    Obstacle    = "O"
    Start       = "S"
    Destination = "D"
    Outside     = "<none>"

def grid_destination_probabilities(_grid,pos,move):
    """
    Given a grid, a current position and a move, this function returns a
    dictionary {Coordinate: probability} of the destinations which will be
    reached with a probability greater than zero and those respective
    probabilities.
    """
    theo_probs = theoretical_destination_probabilities(pos,move)
    probs = {}
    for dest,prob in theo_probs.iteritems():
        prob = theo_probs[dest]
        # Destination outside grid
        if   _grid.outside(dest.x,dest.y):
            probs[pos] = probs.get(pos,0.0) + prob
        # Destination is obstacle
        elif _grid[dest.x][dest.y] is Squares.Obstacle:
            probs[pos] = probs.get(pos,0.0) + prob
        # Destination is reachable
        else:
            probs[dest] = probs.get(dest,0.0) + prob
    return probs

# ------------------------------------------------------------------------------
# Grid rewards

#: The rewards received for moving to a certain type of (orm.model.Squares) grid
#: square. This list also contains the reward for going to Squares.Outside, a 
#: square the robot will never physically reach as it will bounce back, but
#: still necessary to define the reward it receives for moving outside the grid.
grid_rewards = {
        Squares.Empty       : -   10.0,
        Squares.Obstacle    : -  100.0,
        Squares.Start       : -   10.0,
        Squares.Destination :  10000.0,
        Squares.Outside     : -  100.0}

