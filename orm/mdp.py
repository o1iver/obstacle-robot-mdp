# -*- coding: utf-8 -*-
"""
    orm.mdp
    ~~~~~~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import pymdp

import orm.model as model
import orm.util as util

FP_EPSILON = 1e-15

def create_mdp(grid,destination,discount_factor):

    states = map(lambda x: model.Coordinate(x[0],x[1]),list(grid))

    print("States: " + str(states))
    num_states = len(grid)
    print("Number of states:  %i" % num_states)
    state_names = map(lambda c: str(c[0])+'_'+str(c[1]),states)
    print("State names: " + str(state_names))


    actions = model.moves
    print("Actions: " + str(actions))
    num_actions = len(model.moves)
    print("Number of actions: %i" % num_actions)
    action_names = model.move_strings
    print("Action names: " + str(action_names))

    # Initialize to 0.0
    transition_probabilities = util.create_null_array(0.0,num_actions,
            num_states,num_states)

    # Intitialize to -1.0 (Every move costs -1.0)
    rewards = util.create_null_array(-1.0,num_actions,num_states,num_states)

    for sx,state in enumerate(states):
        for ax,action in enumerate(actions):
            theo_probs = model.theoretical_destination_probabilities(state,action)
            grid_probs = model.grid_destination_probabilities(grid,state,action)
            acc = 0.00

            # Set transition probabilities
            for dest,prob in grid_probs.iteritems():
                transition_probabilities[ax][sx][states.index(dest)] = prob
                acc += prob

            # Set reward
            dest = model.move_destination(state,action)
            # Let grid
            if grid.outside(dest.x,dest.y):
                rewards[ax][sx][sx] = model.grid_rewards[model.Squares.Outside]
            # Hit obstacle
            elif grid[dest.x][dest.y] == model.Squares.Obstacle:
                rewards[ax][sx][sx] = model.grid_rewards[model.Squares.Obstacle]
            else:
                rewards[ax][sx][states.index(dest)] = model.grid_rewards[grid[dest.x][dest.y]]

            #stay_reward = 0.0
            ## Set rewards
            #for dest,prob in theo_probs.iteritems():
            #    # Theoretically move outside grid
            #    if grid.outside(dest.x,dest.y):
            #        stay_reward += prob*model.grid_rewards[model.Squares.Outside]
            #    # Theoretically hit obstacle
            #    elif grid[dest.x][dest.y] == model.Squares.Obstacle:
            #        stay_reward += prob*model.grid_rewards[model.Squares.Obstacle]
            #    # Acceptable destination
            #    else:
            #        rw = model.grid_rewards[grid[dest.x][dest.y]]
            #        rewards[ax][sx][states.index(dest)] = rw

            #rewards[ax][sx][sx] = stay_reward

            assert abs(1.0 - acc) < FP_EPSILON

    mdp = pymdp.MDP(tps=transition_probabilities,rws=rewards,
                    state_names=state_names,action_names=action_names,
                    discount=discount_factor)
    return mdp


