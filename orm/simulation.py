# -*- coding: utf-8 -*-
"""
    orm.simulation
    ~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""
import __builtin__
import copy
import codecs
import collections
import json
import random
import re
import select
import sys
import time

import pierre
import pierre.io.signaling

from pierre.io.signaling.signals import StringSignal
from pierre.io.signaling.signals import BooleanSignal
from pierre.io.signaling.signals import FloatSignal
from pierre.io.signaling.signals import IntegerSignal
from pierre.io.signaling.signals import UnsignedIntegerSignal

import pymdp

import orm.grid as _grid
import orm.model as model
import orm.util as util
import orm.mdp as mdp




class Simulation(object):
    """
    This class provides simulation functionality for the roboter and holds
    simulation state.
    """

    #ROBOT_POSITION_ELEM = u"☺"
    ROBOT_POSITION_ELEM = u"X"
    DRAW_ELEM_SIZE = 40

    @staticmethod
    def random_simulation(size_x,size_y,num_obstacles,start,dest,
            state_out_fname=None,auto_draw_fname=None,to_mdp_fname=None,
            mdp_discount=None):
        """
        Create a simulation with the given size and the given number of
        randomly positioned obstacles.
        """
        grid = _grid.Grid.null_grid(model.Squares.Empty,size_x,size_y)

        if grid.outside(start.x,start.y):
            raise ValueError("Start position outside grid!")
        if grid.outside(dest.x,dest.y):
            raise ValueError("Destination outside grid!")

        # Start
        grid[start.x][start.y] = model.Squares.Start

        # Destination
        grid[dest.x][dest.y] = model.Squares.Destination

        # Random obstacles
        c_num_obstacles = 0
        while c_num_obstacles < num_obstacles:
            rand_x = random.randint(0,grid.size_x-1)
            rand_y = random.randint(0,grid.size_y-1)
            if grid[rand_x][rand_y] is model.Squares.Empty:
                grid[rand_x][rand_y] = model.Squares.Obstacle
                c_num_obstacles += 1

        if to_mdp_fname:
            print("Creating MDP model, saving to: %s..." % to_mdp_fname)
            mdp_discount = mdp_discount or 0.9
            mdp_ = mdp.create_mdp(grid,dest,mdp_discount)
            with open(to_mdp_fname,'w') as f:
                pymdp.MDP.writeCassandra(mdp_,f)

        return Simulation(grid,start,dest,state_out_fname,auto_draw_fname)

    def __init__(self,grid,start,dest,state_out_fname=None,auto_draw_fname=None):
        self.grid  = grid
        self.start = start
        self.dest  = dest

        self.current_position = start
        self.grid[start.x][start.y] = model.Squares.Empty

        self.state_out_fname = state_out_fname
        self.auto_draw_fname = auto_draw_fname

        # Number of moves
        self.num_moves  = 0

        # Reward
        self.last_reward  = 0.0
        self.total_reward = 0.0

        self._on_state_change()

    @property
    def arrived(self):
        """
        Boolean property of whether or not the current position is equal to the
        destination.
        """
        return self.current_position == self.dest

    @property
    def state(self):
        """
        Returns a dict containing the values of state variables.
        """
        state = {}
        state["pos_x"]        = self.current_position.x
        state["pos_y"]        = self.current_position.y
        state["num_moves"]    = self.num_moves
        state["last_reward"]  = self.last_reward
        state["total_reward"] = self.total_reward
        return state

    def _on_state_change(self):
        """
        This function should be called whenever the simulation state changes as
        it takes care of any serialization or drawing, that must be updated 
        whenever the state changes.
        """
        # Draw new grid
        if self.auto_draw_fname:
            self.draw(self.auto_draw_fname)

        # Save state
        if self.state_out_fname:
            with open(self.state_out_fname,'w') as f:
                f.write(json.dumps(self.state))

    def move(self,move):
        """
        Simulate a robot move in the given direction. The direction the robot
        will actually move to depends on the model defined in orm.model (see
        the movement probabilities explained there). Given a move, the robot's
        position, as well as the total reward will be updated.
        """

        self.num_moves += 1

        theo_probs = model.theoretical_destination_probabilities(
                self.current_position,move)

        grid_probs = model.grid_destination_probabilities(self.grid,
                self.current_position,move)

        r = random.random()

        t_a = 0.0
        g_a = 0.0

        # Calculate theoretical destination
        for dest,prob in theo_probs.iteritems():
            t_a += prob
            if t_a >= r:
                theo_pos = dest
                print("Robot theoretically moving to: %s" % str(theo_pos))
                break
            if t_a > 1:
                raise RuntimeError("Sum of theoretical move destination"\
                        "probabilities does not equal 1.")

        # Calculate reward
        if self.grid.outside(theo_pos.x,theo_pos.y):
            self.last_reward = model.grid_rewards[model.Squares.Outside]
        else:
            self.last_reward =\
                model.grid_rewards[self.grid[theo_pos.x][theo_pos.y]]
        print("Reward for move: %i" % self.last_reward)
        self.total_reward += self.last_reward

        # Calculate actual destination
        for dest,prob in grid_probs.iteritems():
            g_a += prob
            if g_a >= r:
                self.current_position = dest
                print("Robot moving to: %s" % str(self.current_position))
                break
            if g_a > 1:
                raise RuntimeError("Sum of move destination probabilities"\
                        "does not equal 1.")
        if self.arrived:
            print("Yeah! You have arrived at your destination!")
            print(" ** total number of moves: %i" % self.num_moves)
            print(" ** total reward:          %f" % self.total_reward)

        self._on_state_change()

    def draw(self,fname,grid_size=None):
        """
        Given a file name this function will produce an image of the current
        grid, including obstacles, the destination and the robot's current
        position.
        """
        grid_size = grid_size or self.DRAW_ELEM_SIZE
        elem = self.grid[self.current_position.x][self.current_position.y]
        self.grid[self.current_position.x][self.current_position.y]\
                = self.ROBOT_POSITION_ELEM
        self.grid.draw(fname,grid_size)
        self.grid[self.current_position.x][self.current_position.y]= elem

class SimulationServer(pierre.PierreObject):

    def __init__(self,sp_ident,simulation):
        self.sp_ident = sp_ident
        self.sim = simulation

        # Create signal processing client
        self.spc = pierre.io.signaling.SignalProcessorClient(self.sp_ident)

        # Create move signal
        self.move = StringSignal('orm.sim.move',start_value='Dont',
                read_only=True,processor=self.spc)

        self.pos_x = UnsignedIntegerSignal('orm.sim.pos_x',
                start_value=self.sim.start.x,max=self.sim.grid.size_x-1,
                processor=self.spc)

        self.pos_y = UnsignedIntegerSignal('orm.sim.pos_y',
                start_value=self.sim.start.y,max=self.sim.grid.size_y-1,
                processor=self.spc)

        self.arrived = BooleanSignal('orm.sim.arrived',start_value=False,
                processor=self.spc)

        self.last_reward = FloatSignal('orm.sim.last_reward',
                processor=self.spc)

        self.total_reward = FloatSignal('orm.sim.total_reward',
                processor=self.spc)

        self.last_move_ts = None


    def run(self,epoch_length=1):
        self.running = True
        while not self.sim.arrived:
            move_val,move_ts = self.move.read()

            if move_ts != self.last_move_ts:
                self.last_move_ts = move_ts
                self.sim.move(model.move_from_string(move_val))
                print("Making move: %s" % move_val)
                print("New position: x=%i,y=%i" % (self.sim.current_position.x,
                                                   self.sim.current_position.y))
                self.pos_x.value = self.sim.current_position.x
                self.pos_y.value = self.sim.current_position.y

                self.last_reward.value  = self.sim.last_reward
                self.total_reward.value = self.sim.total_reward
            else:
                print("No new move!")
            time.sleep(epoch_length/2.0)

        self.arrived.value = True


    def loop(self):
        raise NotImplementedError()

if __name__ == "__main__":
    ss = SimulationServer(None,None,sys.stdin,sys.stdout).start()

