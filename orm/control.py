# -*- coding: utf-8 -*-
"""
    orm.control
    ~~~~~~~~~~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import pierre.control
from pierre.scheduling import EveryNMilliSeconds


import pymdp
import pymdp.control

class ORMController(pymdp.control.OnlineMDPController):
    def __init__(self,mdp_fname,io_interface):

        print("Importing MDP from cassandra file...")
        mdp = pymdp.MDP.readCassandra(mdp_fname)

        super(ORMController,self).__init__(mdp=mdp,io_interface=io_interface)

        self.current_state = None
        self.has_sensed = False


    @pierre.control.schedule(EveryNMilliSeconds(2000))
    def act(self):
        if self.has_sensed:
            print("Best action from state %i: %i" % (self.current_state,self.best_action(self.current_state)))
            self.io_interface.act(self.best_action(self.current_state))
    
    @pierre.control.schedule(EveryNMilliSeconds(1000))
    def sense(self):
        self.current_state = self.io_interface.sense()
        print("Current states changed to: %i" % self.current_state)
        if not self.has_sensed:
            self.has_sensed = True

